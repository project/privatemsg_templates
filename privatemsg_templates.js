(function ($) {
  Drupal.behaviors.privatemsg_templates = {
    attach: function (context, settings) {
      $('select#edit-privatemsg-templates', context).bind('change', function() {
        var nid = $(this).val();
        // This check is to prevent an unnecessary get request.
        if (nid > 0) {
          var check = true;
          if($('#privatemsg-new #edit-body-value').val().length) {
            check = confirm('This will overwrite your message, continue?');
          }
          if(check) {
            $('#privatemsg-new #edit-body').attr("disabled", "disabled");
            $.getJSON('/privatemsg_templates/get_template/' + nid, function(json) {
              $('#privatemsg-new #edit-body-value').val(json.data.und[0].value);
              $('#privatemsg-new #edit-body-value').removeAttr("disabled");
            });
          }
        }
        return false;
      });
    }
  }
})(jQuery);